package jp.gp.upload.career.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfiguration {

    /**
     * ログインフィルター
     * @return
     */
    @Bean
    public FilterRegistrationBean<LoginFilter> registerLoginFilter() {
        // FilterをnewしてFilterRegistrationBeanのコンストラクタに渡す
        FilterRegistrationBean<LoginFilter> bean = new FilterRegistrationBean<>(new LoginFilter());
        // Filterのurl-patternを指定
        // ログイン・ログアウト関連以外は対象とする。
        bean.addUrlPatterns("/top");
        bean.addUrlPatterns("/shainSearch/*");
        bean.addUrlPatterns("/shainUpdate/*");

        // Filterの実行順序。整数値の照準に実行される
        bean.setOrder(Integer.MIN_VALUE);
        return bean;
    }

}
