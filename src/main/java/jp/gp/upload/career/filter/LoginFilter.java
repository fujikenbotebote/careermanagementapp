package jp.gp.upload.career.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import jp.gp.upload.career.auth.LoginUser;

/**
 * ログイン済みかどうかチェック
 * 対象のURLはFilterConfigurationで設定する
 * @author Fujimoto Kenta
 */
@Component
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession();
        if (session == null) {
            // セッションが存在しない場合、ログイン画面へリダイレクト
            // ログイン情報が存在しない場合は、ログイン画面へリダイレクト
            String contextPath = request.getServletContext().getContextPath();
            ((HttpServletResponse) response).sendRedirect(contextPath + "/login");
            return;
        }
        LoginUser user = (LoginUser) session.getAttribute(LoginUser.SESSION_KEY);
        if (user == null) {
            // ログイン情報が存在しない場合は、ログイン画面へリダイレクト
            String contextPath = request.getServletContext().getContextPath();
            ((HttpServletResponse) response).sendRedirect(contextPath + "/login");
            return;
        }

        // 次のフィルタへ
        chain.doFilter(request, response);
    }

}
