package jp.gp.upload.career.db.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import jp.gp.upload.career.db.entity.MstSoshiki;

/**
 * 組織マスタ標準Dao実装クラス
 * @author Fujimoto Kenta
 *
 */
@Component
public class MstSoshikiDaoImpl extends NamedParameterJdbcTemplate implements MstSoshikiDao {

	private static final String SELECT = "SELECT * FROM mst_soshiki";

	private static final String SELECT_ID = "SELECT * FROM mst_soshiki WHERE soshiki_id = :SOSHIKI_ID";

	private static final String INSERT = "INSERT INTO mst_soshiki (SOSHIKI_ID, SOSHIKI_NAME, SOSHIKI_ZOKUSEI, OYA_SOSHIKI_ID, HYOJI_JUN, DELETE_FLG, ENTER_TIME, ENTER_USER_ID, UPDATE_TIME, UPDATE_USER_ID) VALUES (:SOSHIKI_ID, :SOSHIKI_NAME, :SOSHIKI_ZOKUSEI, :OYA_SOSHIKI_ID, :HYOJI_JUN, :DELETE_FLG, now(), :ENTER_USER_ID, now(), :UPDATE_USER_ID)";

	private static final String UPDATE = "UPDATE mst_soshiki set SOSHIKI_NAME = :SOSHIKI_NAME, SOSHIKI_ZOKUSEI = :SOSHIKI_ZOKUSEI, OYA_SOSHIKI_ID = :OYA_SOSHIKI_ID, HYOJI_JUN = :HYOJI_JUN, DELETE_FLG = :DELETE_FLG, ENTER_TIME = now(), UPDATE_TIME = now(), UPDATE_USER_ID = :UPDATE_USER_ID WHERE SOSHIKI_ID = :SOSHIKI_ID";

	private static final String DELETE = "DELETE FROM mst_soshiki WHERE SOSHIKI_ID = :SOSHIKI_ID";

	public MstSoshikiDaoImpl(JdbcOperations classicJdbcTemplate) {
		super(classicJdbcTemplate);
	}

	@Override
	public List<MstSoshiki> select() {
		return query(SELECT, new BeanPropertyRowMapper<MstSoshiki>(MstSoshiki.class));
	}

	@Override
	public MstSoshiki selectById(MstSoshiki mstSoshiki) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("SOSHIKI_ID", mstSoshiki.getSoshikiId());
		return queryForObject(SELECT_ID, param, new BeanPropertyRowMapper<MstSoshiki>());}

	@Override
	public int insert(MstSoshiki mstSoshiki) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("SOSHIKI_ID", mstSoshiki.getSoshikiId())
				.addValue("SOSHIKI_NAME", mstSoshiki.getSoshikiName())
				.addValue("SOSHIKI_ZOKUSEI", mstSoshiki.getSoshikiZokusei())
				.addValue("OYA_SOSHIKI_ID", mstSoshiki.getOyaSoshikiId())
				.addValue("HYOJI_JUN", mstSoshiki.getHyojiJun())
				.addValue("DELETE_FLG", mstSoshiki.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstSoshiki.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstSoshiki.getUpdateUserId());
		return update(INSERT, param);
	}

	@Override
	public int insertAutoIncrement(MstSoshiki mstSoshiki) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("SOSHIKI_ID", mstSoshiki.getSoshikiId())
				.addValue("SOSHIKI_NAME", mstSoshiki.getSoshikiName())
				.addValue("SOSHIKI_ZOKUSEI", mstSoshiki.getSoshikiZokusei())
				.addValue("OYA_SOSHIKI_ID", mstSoshiki.getOyaSoshikiId())
				.addValue("HYOJI_JUN", mstSoshiki.getHyojiJun())
				.addValue("DELETE_FLG", mstSoshiki.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstSoshiki.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstSoshiki.getUpdateUserId());
		KeyHolder keyHolder = new GeneratedKeyHolder();
		update(INSERT, param, keyHolder);
		return keyHolder.getKey().intValue();
	}

	@Override
	public int update(MstSoshiki mstSoshiki) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("SOSHIKI_ID", mstSoshiki.getSoshikiId())
				.addValue("SOSHIKI_NAME", mstSoshiki.getSoshikiName())
				.addValue("SOSHIKI_ZOKUSEI", mstSoshiki.getSoshikiZokusei())
				.addValue("OYA_SOSHIKI_ID", mstSoshiki.getOyaSoshikiId())
				.addValue("HYOJI_JUN", mstSoshiki.getHyojiJun())
				.addValue("DELETE_FLG", mstSoshiki.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstSoshiki.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstSoshiki.getUpdateUserId());
		return update(UPDATE, param);
	}

	@Override
	public int delete(MstSoshiki mstSoshiki) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("SOSHIKI_ID", mstSoshiki.getSoshikiId());
		return update(DELETE, param);
	}

}
