package jp.gp.upload.career.db.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import jp.gp.upload.career.db.entity.MstYakushoku;

/**
 * 役職マスタ標準Dao実装クラス
 * @author Fujimoto Kenta
 */
@Component
public class MstYakushokuDaoImpl extends NamedParameterJdbcTemplate implements MstYakushokuDao {

	private static final String SELECT = "SELECT * FROM mst_yakushoku";

	private static final String SELECT_ID = "SELECT * FROM mst_yakushoku WHERE yakushoku_id = :YAKUSHOKU_ID";

	private static final String INSERT = "INSERT INTO mst_yakushoku (YAKUSHOKU_ID, YAKUSHOKU_NAME, YAKUSHOKU_RANK, DELETE_FLG, ENTER_TIME, ENTER_USER_ID, UPDATE_TIME, UPDATE_USER_ID) VALUES (:YAKUSHOKU_ID, :YAKUSHOKU_NAME, :YAKUSHOKU_RANK, :DELETE_FLG, now(), :ENTER_USER_ID, now(), :UPDATE_USER_ID)";

	private static final String UPDATE = "UPDATE mst_yakushoku set YAKUSHOKU_NAME = :YAKUSHOKU_NAME, YAKUSHOKU_RANK = :YAKUSHOKU_RANK, DELETE_FLG = :DELETE_FLG, ENTER_TIME = now(), UPDATE_TIME = now(), UPDATE_USER_ID = :UPDATE_USER_ID WHERE YAKUSHOKU_ID = :YAKUSHOKU_ID";

	private static final String DELETE = "DELETE FROM mst_yakushoku WHERE SOSHIKI_ID = :SOSHIKI_ID";

	public MstYakushokuDaoImpl(JdbcOperations classicJdbcTemplate) {
		super(classicJdbcTemplate);
	}

	@Override
	public List<MstYakushoku> select() {
		return query(SELECT, new BeanPropertyRowMapper<MstYakushoku>(MstYakushoku.class));
	}

	@Override
	public MstYakushoku selectById(MstYakushoku mstYakushoku) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("SHAIN_ID", mstYakushoku.getYakushokuId());
		return queryForObject(SELECT_ID, param, new BeanPropertyRowMapper<MstYakushoku>());
	}

	@Override
	public int insert(MstYakushoku mstSoshiki) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("YAKUSHOKU_ID", mstSoshiki.getYakushokuId())
				.addValue("YAKUSHOKU_NAME", mstSoshiki.getYakushokuName())
				.addValue("YAKUSHOKU_RANK", mstSoshiki.getYakushokuRank())
				.addValue("DELETE_FLG", mstSoshiki.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstSoshiki.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstSoshiki.getUpdateUserId());
		return update(INSERT, param);
	}

	@Override
	public int insertAutoIncrement(MstYakushoku mstSoshiki) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("YAKUSHOKU_ID", mstSoshiki.getYakushokuId())
				.addValue("YAKUSHOKU_NAME", mstSoshiki.getYakushokuName())
				.addValue("YAKUSHOKU_RANK", mstSoshiki.getYakushokuRank())
				.addValue("DELETE_FLG", mstSoshiki.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstSoshiki.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstSoshiki.getUpdateUserId());
		KeyHolder keyHolder = new GeneratedKeyHolder();
		update(INSERT, param, keyHolder);
		return keyHolder.getKey().intValue();
	}

	@Override
	public int update(MstYakushoku mstSoshiki) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("YAKUSHOKU_ID", mstSoshiki.getYakushokuId())
				.addValue("YAKUSHOKU_NAME", mstSoshiki.getYakushokuName())
				.addValue("YAKUSHOKU_RANK", mstSoshiki.getYakushokuRank())
				.addValue("DELETE_FLG", mstSoshiki.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstSoshiki.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstSoshiki.getUpdateUserId());
		return update(UPDATE, param);
	}

	@Override
	public int delete(MstYakushoku mstSoshiki) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("YAKUSHOKU_ID", mstSoshiki.getYakushokuId());
		return update(DELETE, param);
	}

}
