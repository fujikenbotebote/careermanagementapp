package jp.gp.upload.career.db.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 組織マスタエンティティクラス
 * @author Fujimoto Kenta
 */
public class MstSoshiki implements Serializable {

	/** 組織ID */
	private int soshikiId;

	/** 組織名 */
	private String soshikiName;

	/** 組織属性 */
	private String soshikiZokusei;

	/** 親組織ID */
	private int oyaSoshikiId;

	/** 表示順 */
	private int hyojiJun;

	/** 削除フラグ */
	private String deleteFlg;

	/** 登録時刻 */
	private Timestamp enterTime;

	/** 登録ユーザ */
	private int enterUserId;

	/** 更新時刻 */
	private Timestamp updateTime;

	/** 更新ユーザ */
	private int updateUserId;

	public int getSoshikiId() {
		return soshikiId;
	}

	public String getSoshikiName() {
		return soshikiName;
	}

	public String getSoshikiZokusei() {
		return soshikiZokusei;
	}

	public int getOyaSoshikiId() {
		return oyaSoshikiId;
	}

	public int getHyojiJun() {
		return hyojiJun;
	}

	public String getDeleteFlg() {
		return deleteFlg;
	}

	public Timestamp getEnterTime() {
		return enterTime;
	}

	public int getEnterUserId() {
		return enterUserId;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setSoshikiId(int soshikiId) {
		this.soshikiId = soshikiId;
	}

	public void setSoshikiName(String soshikiName) {
		this.soshikiName = soshikiName;
	}

	public void setSoshikiZokusei(String soshikiZokusei) {
		this.soshikiZokusei = soshikiZokusei;
	}

	public void setOyaSoshikiId(int oyaSoshikiId) {
		this.oyaSoshikiId = oyaSoshikiId;
	}

	public void setHyojiJun(int hyojiJun) {
		this.hyojiJun = hyojiJun;
	}

	public void setDeleteFlg(String deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public void setEnterTime(Timestamp enterTime) {
		this.enterTime = enterTime;
	}

	public void setEnterUserId(int enterUserId) {
		this.enterUserId = enterUserId;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}
}
