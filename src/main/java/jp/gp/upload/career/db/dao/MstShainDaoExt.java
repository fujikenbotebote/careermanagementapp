package jp.gp.upload.career.db.dao;

import java.util.List;

import jp.gp.upload.career.db.entity.MstShain;
import jp.gp.upload.career.dto.MstShainExt;

/**
 * 社員マスタ拡張Daoインターフェース
 * @author Fujimoto Kenta
 */
public interface MstShainDaoExt {

    /**
     * 社員番号とパスワードを指定して、社員情報を取得する。
     * 社員情報が取得できない場合はnullを返却する。
     * @param shainNumber 社員番号
     * @param password パスワード（暗号）
     * @return
     */
    MstShainExt selectByNoAndPassword(String shainNumber, String password);

    /**
     * 社員マスタ検索用
     * @param shainName 氏名
     * @param soshikiName 組織名
     * @param searchMaxCount 検索最大件数 0件以上の場合は設定する
     * @return
     */
    List<MstShainExt> selectMstSearch(String shainName, String soshikiName, int searchMaxCount);

    /**
     * 社員マスタ検索（削除フラグ考慮）
     * @param shainId 社員ID
     * @return
     */
    MstShain selectAvailable(int shainId);

    /**
     * 社員情報の論理削除を行う
     * @param shainId 社員ID
     * @param loginUserId ログインユーザID
     * @return
     */
    int logicalDelete(int shainId, int loginUserId);

}
