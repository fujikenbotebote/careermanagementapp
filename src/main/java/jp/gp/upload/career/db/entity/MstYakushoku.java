package jp.gp.upload.career.db.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 役職マスタエンティティクラス
 * @author Fujimoto Kenta
 */
public class MstYakushoku implements Serializable {

	/** 役職ID */
	private int yakushokuId;

	/** 役職名 */
	private String yakushokuName;

	/** ランク */
	private int yakushokuRank;

	/** 削除フラグ */
	private String deleteFlg;

	/** 登録時刻 */
	private Timestamp enterTime;

	/** 登録ユーザ */
	private int enterUserId;

	/** 更新時刻 */
	private Timestamp updateTime;

	/** 更新ユーザ */
	private int updateUserId;

	public int getYakushokuId() {
		return yakushokuId;
	}

	public String getYakushokuName() {
		return yakushokuName;
	}

	public int getYakushokuRank() {
		return yakushokuRank;
	}

	public String getDeleteFlg() {
		return deleteFlg;
	}

	public Timestamp getEnterTime() {
		return enterTime;
	}

	public int getEnterUserId() {
		return enterUserId;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setYakushokuId(int yakushokuId) {
		this.yakushokuId = yakushokuId;
	}

	public void setYakushokuName(String yakushokuName) {
		this.yakushokuName = yakushokuName;
	}

	public void setYakushokuRank(int yakushokuRank) {
		this.yakushokuRank = yakushokuRank;
	}

	public void setDeleteFlg(String deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public void setEnterTime(Timestamp enterTime) {
		this.enterTime = enterTime;
	}

	public void setEnterUserId(int enterUserId) {
		this.enterUserId = enterUserId;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}
}
