package jp.gp.upload.career.db.dao;

import jp.gp.upload.career.db.entity.MstShain;

/**
 * 社員マスタ標準Daoインターフェース
 * @author Fujimoto Kenta
 */
public interface MstShainDao extends Dao<MstShain> {

}
