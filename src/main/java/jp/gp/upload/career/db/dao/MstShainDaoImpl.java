package jp.gp.upload.career.db.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import jp.gp.upload.career.db.entity.MstShain;

/**
 * MST_SHAINテーブル標準DAO
 * @author Fujimoto Kenta
 */
@Component
public class MstShainDaoImpl extends NamedParameterJdbcTemplate implements MstShainDao {

	private static final String SELECT = "SELECT * FROM mst_shain";

	private static final String SELECT_ID = "SELECT * FROM mst_shain WHERE shain_id = :SHAIN_ID";

	private static final String INSERT = "INSERT INTO mst_shain (SHAIN_ID, SHAIN_NO, SHAIN_NAME, SEX, SHOZOKU_SOSHIKI_ID, YAKUSHOKU_ID, BIRTHDAY, EMAIL_ADDRESS, PHONE_NUMBER, NYUSHABI, TAISHABI, ROLE_ID, DELETE_FLG, ENTER_TIME, ENTER_USER_ID, UPDATE_TIME, UPDATE_USER_ID) VALUES (:SHAIN_ID, :SHAIN_NO, :SHAIN_NAME, :SEX, :SHOZOKU_SOSHIKI_ID, :YAKUSHOKU_ID, :BIRTHDAY, :EMAIL_ADDRESS, :PHONE_NUMBER, :NYUSHABI, :TAISHABI, :ROLE_ID, :DELETE_FLG, now(), :ENTER_USER_ID, now(), :UPDATE_USER_ID)";

	private static final String UPDATE = "UPDATE mst_shain set SHAIN_NO = :SHAIN_NO, SHAIN_NAME = :SHAIN_NAME, SEX = :SEX, SHOZOKU_SOSHIKI_ID = :SHOZOKU_SOSHIKI_ID, YAKUSHOKU_ID = :YAKUSHOKU_ID, BIRTHDAY = :BIRTHDAY, EMAIL_ADDRESS = :EMAIL_ADDRESS, PHONE_NUMBER = :PHONE_NUMBER, NYUSHABI = :NYUSHABI, TAISHABI = :TAISHABI, ROLE_ID = :ROLE_ID, DELETE_FLG = :DELETE_FLG, UPDATE_TIME = now(), UPDATE_USER_ID = :UPDATE_USER_ID WHERE shain_id = :SHAIN_ID";

	private static final String DELETE = "DELETE FROM mst_shain WHERE shain_id = :SHAIN_ID";

	public MstShainDaoImpl(JdbcOperations classicJdbcTemplate) {
		super(classicJdbcTemplate);
	}

	public List<MstShain> select() {
		return query(SELECT, new BeanPropertyRowMapper<MstShain>(MstShain.class));
	}

	public MstShain selectById(MstShain mstShain) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("SHAIN_ID", mstShain.getShainId());
		return queryForObject(SELECT_ID, param, new BeanPropertyRowMapper<MstShain>());
	}

	public int insert(MstShain mstShain) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("SHAIN_ID", mstShain.getShainId())
				.addValue("SHAIN_NO", mstShain.getShainNo())
				.addValue("SHAIN_NAME", mstShain.getShainName())
				.addValue("SEX", mstShain.getSex())
				.addValue("SHOZOKU_SOSHIKI_ID", mstShain.getShozokuSoshikiId())
				.addValue("YAKUSHOKU_ID", mstShain.getYakushokuId())
				.addValue("BIRTHDAY", mstShain.getBirthday())
				.addValue("EMAIL_ADDRESS", mstShain.getEmailAddress())
				.addValue("PHONE_NUMBER", mstShain.getPhoneNumber())
				.addValue("NYUSHABI", mstShain.getNyushabi())
				.addValue("TAISHABI", mstShain.getTaishabi())
				.addValue("ROLE_ID", mstShain.getRoleId())
				.addValue("DELETE_FLG", mstShain.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstShain.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstShain.getUpdateUserId());
		return update(INSERT, param);
	}

	@Override
	public int insertAutoIncrement(MstShain mstShain) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("SHAIN_NO", mstShain.getShainNo())
				.addValue("SHAIN_NAME", mstShain.getShainName())
				.addValue("SEX", mstShain.getSex())
				.addValue("SHOZOKU_SOSHIKI_ID", mstShain.getShozokuSoshikiId())
				.addValue("YAKUSHOKU_ID", mstShain.getYakushokuId())
				.addValue("BIRTHDAY", mstShain.getBirthday())
				.addValue("EMAIL_ADDRESS", mstShain.getEmailAddress())
				.addValue("PHONE_NUMBER", mstShain.getPhoneNumber())
				.addValue("NYUSHABI", mstShain.getNyushabi())
				.addValue("TAISHABI", mstShain.getTaishabi())
				.addValue("ROLE_ID", mstShain.getRoleId())
				.addValue("DELETE_FLG", mstShain.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstShain.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstShain.getUpdateUserId());
		KeyHolder keyHolder = new GeneratedKeyHolder();
		update(INSERT, param, keyHolder);
		return keyHolder.getKey().intValue();
	}

	public int update(MstShain mstShain) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("SHAIN_ID", mstShain.getShainId())
				.addValue("SHAIN_NO", mstShain.getShainNo())
				.addValue("SHAIN_NAME", mstShain.getShainName())
				.addValue("SEX", mstShain.getSex())
				.addValue("SHOZOKU_SOSHIKI_ID", mstShain.getShozokuSoshikiId())
				.addValue("YAKUSHOKU_ID", mstShain.getYakushokuId())
				.addValue("BIRTHDAY", mstShain.getBirthday())
				.addValue("EMAIL_ADDRESS", mstShain.getEmailAddress())
				.addValue("PHONE_NUMBER", mstShain.getPhoneNumber())
				.addValue("NYUSHABI", mstShain.getNyushabi())
				.addValue("TAISHABI", mstShain.getTaishabi())
				.addValue("ROLE_ID", mstShain.getRoleId())
				.addValue("DELETE_FLG", mstShain.getDeleteFlg())
				.addValue("ENTER_USER_ID", mstShain.getEnterUserId())
				.addValue("UPDATE_USER_ID", mstShain.getUpdateUserId());
		return update(UPDATE, param);
	}

	public int delete(MstShain mstShain) {
		MapSqlParameterSource param = new MapSqlParameterSource()
				.addValue("SHAIN_ID", mstShain.getShainId());
		return update(DELETE, param);
	}
}
