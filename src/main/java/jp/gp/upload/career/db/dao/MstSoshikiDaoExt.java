package jp.gp.upload.career.db.dao;

import java.util.List;

import jp.gp.upload.career.db.entity.MstSoshiki;

/**
 * 組織マスタ拡張Daoインターフェース
 * @author Fujimoto Kenta
 */
public interface MstSoshikiDaoExt {

    /**
     * 組織マスタの削除フラグを考慮した全権検索を行う
     * @return
     */
    List<MstSoshiki> selectAllAvailable();

    /**
     * 指定した組織を含む配下の組織を取得する
     * @param soshikiId 組織ID
     * @return 配下の組織情報
     */
    List<MstSoshiki> selectSub(int soshikiId);
}
