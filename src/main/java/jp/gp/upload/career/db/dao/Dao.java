package jp.gp.upload.career.db.dao;

import java.util.List;

/**
 * 標準Daoのインターフェース
 * CRUDを定義する
 * @author kenta
 *
 * @param <T> 対応するテーブルのEntityクラス
 */
public interface Dao<T> {

	/**
	 * 条件を指定しない全権検索
	 * @return
	 */
	public List<T> select();

	/**
	 * 主キーを指定する検索
	 * @param entity エンティティ
	 * @return
	 */
	public T selectById(T entity);

	/**
	 * 新規登録
	 * @param entity
	 * @return
	 */
	public int insert(T entity);

	/**
	 * 新規登録
	 * 戻り値としてオートインクリメント値を返す
	 * @param entity
	 * @return
	 */
	public int insertAutoIncrement(T entity);

	/**
	 * 更新
	 * @param entity
	 * @return
	 */
	public int update(T entity);

	/**
	 * 削除
	 * @param entity
	 * @return
	 */
	public int delete(T entity);
}
