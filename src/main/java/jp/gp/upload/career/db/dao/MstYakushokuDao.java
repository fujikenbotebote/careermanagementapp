package jp.gp.upload.career.db.dao;

import jp.gp.upload.career.db.entity.MstYakushoku;

/**
 * 役職マスタ標準Daoインターフェース
 * @author Fujimoto Kenta
 */
public interface MstYakushokuDao extends Dao<MstYakushoku> {

}
