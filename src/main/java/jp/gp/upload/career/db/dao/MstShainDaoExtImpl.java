package jp.gp.upload.career.db.dao;

import static jp.gp.upload.career.constant.CareerConst.FLG_OFF;
import static jp.gp.upload.career.constant.CareerConst.FLG_ON;
import static jp.gp.upload.career.constant.CareerConst.ROLE.USER;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import jp.gp.upload.career.db.entity.MstShain;
import jp.gp.upload.career.dto.MstShainExt;
import jp.gp.upload.career.util.SqlUtil;

/**
 * MST_SHAINテーブル拡張DAO
 * @author Fujimoto Kenta
 */
@Component
public class MstShainDaoExtImpl extends NamedParameterJdbcTemplate implements MstShainDaoExt {

    public MstShainDaoExtImpl(DataSource dataSource) {
        super(dataSource);
    }

    private static final String SELECT_BY_NUMBER_AND_PASSWORD = ""
            + "SELECT "
            + "		t1.shain_id "
            + ",	t1.shain_no "
            + ",	t1.shain_name "
            + ",	t1.sex "
            + ",	t1.shozoku_soshiki_id "
            + ",	t1.yakushoku_id "
            + ",	t1.birthday "
            + ",	t1.email_address "
            + ",	t1.phone_number "
            + ",	t1.nyushabi "
            + ",	t1.taishabi "
            + ",	t1.role_id "
            + ",	t1.delete_flg "
            + ",	t2.soshiki_name "
            + ",	t2.soshiki_zokusei "
            + ",	t3.yakushoku_name "
            + ",	t3.yakushoku_rank "
            + "FROM "
            + "		mst_shain t1 "
            + "INNER JOIN mst_soshiki t2 "
            + "  ON  t1.shozoku_soshiki_id = t2.soshiki_id "
            + "INNER JOIN mst_yakushoku t3 "
            + "  ON  t1.yakushoku_id = t3.yakushoku_id "
            + "WHERE "
            + "		t1.shain_no = :SHAIN_NO "
            + "	AND	t1.password = :PASSWORD "
            + "	AND	t1.delete_flg = '0' ";

    @Override
    public MstShainExt selectByNoAndPassword(String shainNo, String password) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("SHAIN_NO", shainNo);
        param.put("PASSWORD", password);
        try {
            return queryForObject(SELECT_BY_NUMBER_AND_PASSWORD, param,
                    new BeanPropertyRowMapper<MstShainExt>(MstShainExt.class));
        } catch (EmptyResultDataAccessException e) {
            // queryForObjectは0件の場合、エラーとなるのでcatchしてnullを返す
            return null;
        }
    }

    private static final String SELECT_MST_SEARCH = ""
            + "SELECT "
            + "     t1.shain_id "
            + ",    t1.shain_no "
            + ",    t1.shain_name "
            + ",    t2.soshiki_name "
            + ",    t2.soshiki_zokusei "
            + "FROM "
            + "     mst_shain t1 "
            + "     INNER JOIN mst_soshiki t2 "
            + "     ON t1.shozoku_soshiki_id = t2.soshiki_id "
            + "WHERE "
            + "     t1.delete_flg = :FLG_OFF "
            + "AND  t1.role_id = :USER ";

    @Override
    public List<MstShainExt> selectMstSearch(String shainName, String soshikiName, int searchMaxCount) {
        Map<String, Object> param = new HashMap<String, Object>();

        StringBuilder sb = new StringBuilder();
        sb.append(SELECT_MST_SEARCH);
        // 氏名が設定されている場合、検索条件に設定する。
        if (shainName != null && !shainName.isEmpty()) {
            sb.append("AND t1.shain_name LIKE :SHAIN_NAME ");
            param.put("SHAIN_NAME", SqlUtil.partialMatch(shainName));
        }
        // 組織名が設定されている場合、検索条件に設定する。
        if (soshikiName != null && !soshikiName.isEmpty()) {
            sb.append("AND t2.soshiki_name LIKE :SOSHIKI_NAME ");
            param.put("SOSHIKI_NAME", SqlUtil.partialMatch(soshikiName));
        }
        // 最大件数設定
        if (searchMaxCount > 0) {
            // 最大件数＋１を設定する
            sb.append("LIMIT :SEARCH_MAX_NUM ");
            param.put("SEARCH_MAX_NUM", searchMaxCount + 1);
        }
        param.put("FLG_OFF", FLG_OFF);
        param.put("USER", USER.getRoleId());
        return query(sb.toString(), param,
                new BeanPropertyRowMapper<MstShainExt>(MstShainExt.class));
    }

    private static final String SELECT_AVAILABLE = ""
            + "SELECT * "
            + "FROM mst_shain "
            + "WHERE shain_id = :SHAIN_ID "
            + "AND delete_flg = :FLG_OFF";

    @Override
    public MstShain selectAvailable(int shainId) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("SHAIN_ID", shainId);
        param.put("FLG_OFF", FLG_OFF);
        return queryForObject(SELECT_AVAILABLE, param, new BeanPropertyRowMapper<MstShain>(MstShain.class));
    }

    private static final String LOGICAL_DELETE = ""
            + "UPDATE mst_shain "
            + "SET "
            + "     delete_flg = :DELETE "
            + ",    update_time = now() "
            + ",    update_user_id = :LOGIN_USER_ID "
            + "WHERE "
            + "     shain_id = :SHAIN_ID ";

    @Override
    public int logicalDelete(int shainId, int loginUserId) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("DELETE", FLG_ON);
        paramMap.put("SHAIN_ID", shainId);
        paramMap.put("LOGIN_USER_ID", loginUserId);
        return update(LOGICAL_DELETE, paramMap);
    }
}
