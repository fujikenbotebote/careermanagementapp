package jp.gp.upload.career.db.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * 社員マスタエンティティクラス
 * @author Fujimoto Kenta
 */
public class MstShain implements Serializable {

	/** 社員ID */
	private int shainId;

	/** 社員番号 */
	private String shainNo;

	/** 氏名 */
	private String shainName;

	/** 性別 */
	private String sex;

	/** 所属組織ID */
	private int shozokuSoshikiId;

	/** 役職ID */
	private int yakushokuId;

	/** 生年月日 */
	private Date birthday;

	/** メールアドレス */
	private String emailAddress;

	/** 電話番号 */
	private String phoneNumber;

	/** 入社日 */
	private Date nyushabi;

	/** 退社日 */
	private Date taishabi;

	/** ロールID */
	private int roleId;

	/** 削除フラグ */
	private String deleteFlg;

	/** 登録時刻 */
	private Timestamp enterTime;

	/** 登録ユーザ */
	private int enterUserId;

	/** 更新時刻 */
	private Timestamp updateTime;

	/** 更新ユーザ */
	private int updateUserId;

	public int getShainId() {
		return shainId;
	}

	public String getShainNo() {
		return shainNo;
	}

	public String getShainName() {
		return shainName;
	}

	public String getSex() {
		return sex;
	}

	public int getShozokuSoshikiId() {
		return shozokuSoshikiId;
	}

	public int getYakushokuId() {
		return yakushokuId;
	}

	public Date getBirthday() {
		return birthday;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public Date getNyushabi() {
		return nyushabi;
	}

	public Date getTaishabi() {
		return taishabi;
	}

	public int getRoleId() {
		return roleId;
	}

	public String getDeleteFlg() {
		return deleteFlg;
	}

	public Timestamp getEnterTime() {
		return enterTime;
	}

	public int getEnterUserId() {
		return enterUserId;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setShainId(int shainId) {
		this.shainId = shainId;
	}

	public void setShainNo(String shainNo) {
		this.shainNo = shainNo;
	}

	public void setShainName(String shainName) {
		this.shainName = shainName;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setShozokuSoshikiId(int shozokuSoshikiId) {
		this.shozokuSoshikiId = shozokuSoshikiId;
	}

	public void setYakushokuId(int yakushokuId) {
		this.yakushokuId = yakushokuId;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setNyushabi(Date nyushabi) {
		this.nyushabi = nyushabi;
	}

	public void setTaishabi(Date taishabi) {
		this.taishabi = taishabi;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public void setDeleteFlg(String deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public void setEnterTime(Timestamp enterTime) {
		this.enterTime = enterTime;
	}

	public void setEnterUserId(int enterUserId) {
		this.enterUserId = enterUserId;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

}
