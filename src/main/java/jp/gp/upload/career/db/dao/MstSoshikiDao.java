package jp.gp.upload.career.db.dao;

import jp.gp.upload.career.db.entity.MstSoshiki;

/**
 * 組織マスタ標準Daoインターフェース
 * @author Fujimoto Kenta
 */
public interface MstSoshikiDao extends Dao<MstSoshiki> {

}
