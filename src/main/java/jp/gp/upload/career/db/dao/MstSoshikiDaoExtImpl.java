package jp.gp.upload.career.db.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import jp.gp.upload.career.constant.CareerConst;
import jp.gp.upload.career.db.entity.MstSoshiki;

/**
 * 組織マスタ拡張Dao実装クラス
 * @author Fujimoto Kenta
 *
 */
@Component
public class MstSoshikiDaoExtImpl extends NamedParameterJdbcTemplate implements MstSoshikiDaoExt {

    public MstSoshikiDaoExtImpl(DataSource dataSource) {
        super(dataSource);
    }

    // 削除フラグを考慮した組織マスタ全件検索のSQL
    private static final String SELECT_ALL_AVAILABLE = "" //
            + "SELECT * "
            + "FROM mst_soshiki "
            + "WHERE delete_flg = :DELETE_FLG ";

    @Override
    public List<MstSoshiki> selectAllAvailable() {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("DELETE_FLG", CareerConst.FLG_OFF);
        return query(SELECT_ALL_AVAILABLE, param, new BeanPropertyRowMapper<MstSoshiki>(MstSoshiki.class));
    }

    private static final String SELECT_SUB = "" //
            + "WITH RECURSIVE " //
            + "  soshiki AS ( " //
            + "    SELECT " //
            + "        soshiki_id " //
            + "       ,soshiki_name " //
            + "    FROM mst_soshiki " //
            + "    WHERE " //
            + "        soshiki_id = :SOSHIKI_ID " //
            + "    AND delete_flg = :DELETE_FLG " //
            + "  UNION ALL " //
            + "    SELECT " //
            + "        child.soshiki_id " //
            + "       ,child.soshiki_name " //
            + "    FROM mst_soshiki child " //
            + "      INNER JOIN soshiki " //
            + "      ON  child.oya_soshiki_id = soshiki.soshiki_id " //
            + "      WHERE " //
            + "          child.delete_flg = 0 " //
            + ")  " //
            + "SELECT * " //
            + "FROM soshiki ";

    public List<MstSoshiki> selectSub(int soshikiId) {

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("DELETE_FLG", CareerConst.FLG_OFF);
        param.put("SOSHIKI_ID", soshikiId);
        return query(SELECT_ALL_AVAILABLE, param, new BeanPropertyRowMapper<MstSoshiki>(MstSoshiki.class));
    }
}
