package jp.gp.upload.career.db.dao;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * 役職マスタ拡張Dao実装クラス
 * @author Fujimoto Kenta
 */
@Component
public class MstYakushokuDaoExtImpl extends NamedParameterJdbcTemplate implements MstYakushokuDaoExt {

	public MstYakushokuDaoExtImpl(JdbcOperations classicJdbcTemplate) {
		super(classicJdbcTemplate);
	}

}
