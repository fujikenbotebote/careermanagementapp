package jp.gp.upload.career.util;

public class SqlUtil {

    /**
     * 部分一致文字列作成
     * @param condition 検索条件
     * @return
     */
    public static String partialMatch(String condition) {
        return "%" + escape(condition) + "%";
    }

    /**
     * 前方一致文字列作成
     * @param condition 検索条件
     * @return
     */
    public static String forwardMatch(String condition) {
        return escape(condition) + "%";
    }

    /**
     * 後方一致文字列作成
     * @param condition 検索条件
     * @return
     */
    public static String backwardMatch(String condition) {
        return "%" + escape(condition);
    }

    /**
     * SQLエスケープ処理
     * %と_に\を付与する
     * @param condition 検索条件
     * @return
     */
    private static String escape(String condition) {
        return condition.replaceAll("_", "\\_").replace("%", "\\%");
    }
}
