package jp.gp.upload.career.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordUtil {

    // Hash化するメソッド
    private static final String ENCODE = "SHA-256";

    // ソルト
    private static final String SULT = "C)$N!POu2_[1(VD7d9I";

    /**
     * 平文のパスワードをハッシュ暗号化する
     * @param plainPassword パスワード
     * @return
     */
    public static String encode(String plainPassword) {
        if (plainPassword == null) {
            throw new IllegalArgumentException();
        }
        MessageDigest md;
        try {
            md = MessageDigest.getInstance(ENCODE);
        } catch (NoSuchAlgorithmException e) {
            // ありえない
            throw new RuntimeException(e);
        }
        String pre = plainPassword + SULT;
        md.update(pre.getBytes());
        byte[] cipher_byte = md.digest();
        StringBuilder sb = new StringBuilder(2 * cipher_byte.length);
        for (byte b : cipher_byte) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }

}
