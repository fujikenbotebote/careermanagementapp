package jp.gp.upload.career.auth;

import static jp.gp.upload.career.constant.CareerConst.YAKUSHOKU.*;
import static jp.gp.upload.career.constant.CareerConst.ZOKUSEI.*;

import java.io.Serializable;
import java.sql.Date;

import jp.gp.upload.career.constant.CareerConst.ROLE;
import jp.gp.upload.career.dto.MstShainExt;

/**
 * ログインユーザ情報
 * @author Fujimoto Kenta
 *
 */
public class LoginUser implements Serializable {

    public static final String SESSION_KEY = "LoginUserSessionKey";

    /** 社員ID */
    private int shainId;

    /** 社員番号 */
    private String shainNo;

    /** 氏名 */
    private String shainName;

    /** 性別 */
    private String sex;

    /** 所属組織ID */
    private int shozokuSoshikiId;

    /** 組織名 */
    private String soshikiName;

    /** 組織属性 */
    private String soshikiZokusei;

    /** 役職ID */
    private int yakushokuId;

    /** 役職名 */
    private String yakushokuName;

    /** 役職ランク */
    private int yakushokuRank;

    /** 生年月日 */
    private Date birthday;

    /** メールアドレス */
    private String emailAddress;

    /** 電話番号 */
    private String phoneNumber;

    /** 入社日 */
    private Date nyushabi;

    /** 退社日 */
    private Date taishabi;

    /** ロールID */
    private int roleId;

    /**
     * 社員情報を引数に取るコンストラクタ
     * @param shain 社員情報
     */
    public LoginUser(MstShainExt shain) {
        this.shainId = shain.getShainId();
        this.shainNo = shain.getShainNo();
        this.shainName = shain.getShainName();
        this.sex = shain.getSex();
        this.shozokuSoshikiId = shain.getShozokuSoshikiId();
        this.soshikiName = shain.getSoshikiName();
        this.soshikiZokusei = shain.getSoshikiZokusei();
        this.yakushokuId = shain.getYakushokuId();
        this.yakushokuName = shain.getYakushokuName();
        this.yakushokuRank = shain.getYakushokuRank();
        this.birthday = shain.getBirthday();
        this.emailAddress = shain.getEmailAddress();
        this.phoneNumber = shain.getPhoneNumber();
        this.nyushabi = shain.getNyushabi();
        this.taishabi = shain.getTaishabi();
        this.roleId = shain.getRoleId();
    }

    public int getShainId() {
        return shainId;
    }

    public String getShainNo() {
        return shainNo;
    }

    public String getShainName() {
        return shainName;
    }

    public String getSex() {
        return sex;
    }

    public int getShozokuSoshikiId() {
        return shozokuSoshikiId;
    }

    public String getSoshikiName() {
        return soshikiName;
    }

    public String getSoshikiZokusei() {
        return soshikiZokusei;
    }

    public int getYakushokuId() {
        return yakushokuId;
    }

    public String getYakushokuName() {
        return yakushokuName;
    }

    public int getYakushokuRank() {
        return yakushokuRank;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Date getNyushabi() {
        return nyushabi;
    }

    public Date getTaishabi() {
        return taishabi;
    }

    public int getRoleId() {
        return roleId;
    }

    /**
     * 営業かどうか
     * @return
     */
    public boolean isEigyo() {
        return soshikiZokusei.equals(EIGYO.getZokusei());
    }

    /**
     * 役職ランクが係長以上かどうか
     * @return
     */
    public boolean isKakaricho() {
        return yakushokuRank >= KAKARICHO.getYakushokuRank();
    }

    /**
     * 役職ランクが役員以上かどうか
     * @return
     */
    public boolean isYakuin() {
        return yakushokuRank >= YAKUIN.getYakushokuRank();
    }

    /**
     * 管理者かどうか
     * @return
     */
    public boolean isAdmin() {
        return roleId == ROLE.ADMIN.getRoleId();
    }
}
