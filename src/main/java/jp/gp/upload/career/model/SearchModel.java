package jp.gp.upload.career.model;

import java.io.Serializable;
import java.util.List;

public class SearchModel<T> implements Serializable {

    private String message;

    private List<T> searchResult;

    public String getMessage() {
        return message;
    }

    public List<T> getSearchResult() {
        return searchResult;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSearchResult(List<T> searchResult) {
        this.searchResult = searchResult;
    }

    public int getCount() {
        if (searchResult == null) {
            return 0;
        }
        return searchResult.size();
    }

}
