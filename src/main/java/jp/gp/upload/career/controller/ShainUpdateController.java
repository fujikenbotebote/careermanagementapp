package jp.gp.upload.career.controller;

import static jp.gp.upload.career.constant.CareerConst.VIEW_NAME_SHIAN_UPDATE;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jp.gp.upload.career.constant.CareerConst;
import jp.gp.upload.career.dto.ShainUpdateDto;
import jp.gp.upload.career.logic.ShainUpdateLogic;

@Controller
public class ShainUpdateController {

    @Autowired
    ShainUpdateLogic shainUpdateLogic;

    /**
     * 社員マスタ登録画面初期表示
     * @parma shainNo 社員ID
     * @return
     */
    @RequestMapping("/shainUpdate")
    public ModelAndView init(@RequestParam(required = false) String shainId) {
        ModelAndView mv = new ModelAndView(VIEW_NAME_SHIAN_UPDATE);
        Map<String, Object> model = mv.getModel();
        ShainUpdateDto shainMst = null;
        // 社員IDがnullの場合は新規登録、設定されている場合は更新
        if (shainId != null && shainId.length() != 0) {
            model.put(CareerConst.DISP_MODE_KEY, CareerConst.DISP_MODE_UPDATE);
            shainMst = shainUpdateLogic.getShain(Integer.parseInt(shainId));
            model.put("shain", shainMst);
        } else {
            model.put(CareerConst.DISP_MODE_KEY, CareerConst.DISP_MODE_CREATE);
            shainMst = new ShainUpdateDto();
            model.put("shain", shainMst);
        }

        return mv;
    }

}
