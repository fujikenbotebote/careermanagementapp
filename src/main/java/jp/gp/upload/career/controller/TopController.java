package jp.gp.upload.career.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.gp.upload.career.logic.LoginLogic;

@Controller
public class TopController {

	/** ロガー */
	private static final Logger log = LoggerFactory.getLogger(TopController.class);

	@Autowired
	private LoginLogic loginLogic;

    @Autowired
    MessageSource mssageSource;

	/**
	 * トップ画面初期表示
	 * @param session
	 * @return
	 */
	@RequestMapping("top")
	public String init(HttpSession session) {
		// お知らせ一覧を取得する
		// ログイン画面表示（jsp名称と合わせる）
		return "top";
	}

}
