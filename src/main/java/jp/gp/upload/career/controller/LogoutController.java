package jp.gp.upload.career.controller;

import static jp.gp.upload.career.constant.CareerConst.DEFAULT_LOCALE;
import static jp.gp.upload.career.constant.CareerConst.VIEW_NAME_LOGIN;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import jp.gp.upload.career.form.LoginForm;

@Controller
public class LogoutController {

    /** ロガー */
    private static final Logger log = LoggerFactory.getLogger(LogoutController.class);

    @Autowired
    MessageSource mssageSource;

    /**
     * ログアウト処理
     * @param session
     * @return
     */
    @RequestMapping("logout")
    public ModelAndView logout(HttpSession session) {
        // セッション破棄
        session.invalidate();
        ModelAndView mv = new ModelAndView();
        LoginForm form = new LoginForm();
        // メッセージ設定
        form.setErrorMessage(mssageSource.getMessage("login.incollectNoPassword", new Object[] {}, DEFAULT_LOCALE));
        // ログイン画面へ
        mv.setViewName(VIEW_NAME_LOGIN);
        mv.getModelMap().put("message",
                mssageSource.getMessage("logout.execute", new Object[] {}, DEFAULT_LOCALE));
        return mv;
    }

}
