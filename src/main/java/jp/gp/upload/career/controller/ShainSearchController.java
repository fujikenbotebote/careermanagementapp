package jp.gp.upload.career.controller;

import static jp.gp.upload.career.constant.CareerConst.VIEW_NAME_SHAIN_SEARCH;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jp.gp.upload.career.auth.LoginUser;
import jp.gp.upload.career.constant.CareerConst;
import jp.gp.upload.career.dto.MstShainExt;
import jp.gp.upload.career.form.ShainSearchForm;
import jp.gp.upload.career.logic.ShainSearchLogic;
import jp.gp.upload.career.model.SearchModel;
import jp.gp.upload.career.properties.CareerProperties;

/**
 * 社員マスタ検索画面のコントローラクラス
 * @author Fujimoto Kenta
 *
 */
@Controller
public class ShainSearchController {

    @Autowired
    private ShainSearchLogic shainSearchLogic;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CareerProperties careerProperties;

    /**
     * 社員マスタ検索画面初期表示
     * @return
     */
    @RequestMapping("shainSearch")
    public String init(HttpSession session) {
        // 直接アクセス対策
        // 営業もしくは係長以上でない場合は利用不可
        LoginUser loginUser = (LoginUser) session.getAttribute(LoginUser.SESSION_KEY);
        if (!loginUser.isEigyo() && !loginUser.isKakaricho()) {
            // エラー画面へ遷移
            return "403";
        }
        // 社員マスタ検索画面表示（jsp名称と合わせる）
        return VIEW_NAME_SHAIN_SEARCH;
    }

    /**
     * 社員マスタ検索
     * @param shainName 氏名
     * @param soshikiName 組織名
     * @return 検索結果
     */
    @RequestMapping("shainSearch/search")
    @ResponseBody // JSPではなく、JSONを返す
    public Object search(@Validated ShainSearchForm form, BindingResult result) {
        // TODO 検索条件チェック
        SearchModel<MstShainExt> model = new SearchModel<MstShainExt>();
        // 検索
        List<MstShainExt> shainList = shainSearchLogic.search(form.getShainName(), form.getSoshikiName(),
                careerProperties.maxSearchCount);
        if (shainList.isEmpty()) {
            // 検索結果が0件の場合エラーメッセージを設定する。
            model.setMessage(messageSource.getMessage("shainSearch.emptyResult", null, CareerConst.DEFAULT_LOCALE));
        } else if (shainList.size() > careerProperties.maxSearchCount) {
            // 検索結果が最大件数を超える場合、エラーメッセージを設定する。
            model.setMessage(
                    messageSource.getMessage("shainSearch.tooMuchHitResult", null, CareerConst.DEFAULT_LOCALE));
        } else {
            model.setSearchResult(shainList);
        }
        return model;
    }

    /**
     * 社員削除
     * @param shainId 社員ID
     * @return
     */
    @RequestMapping("shainSearch/delete")
    @ResponseBody // JSPではなく、JSONを返す
    public Map<String, Object> delete(@RequestParam int shainId, HttpSession session) {
        LoginUser loginUser = (LoginUser) session.getAttribute(LoginUser.SESSION_KEY);
        // TODO 入力チェック
        Map<String, Object> result = new HashMap<String, Object>();
        if (!shainSearchLogic.existsShain(shainId)) {
            result.put("errorMessage",
                    messageSource.getMessage("shainSearch.alreadyDeleted", null, CareerConst.DEFAULT_LOCALE));
            return result;
        }
        // 社員情報削除
        shainSearchLogic.delete(shainId, loginUser.getShainId());
        result.put("errorMessage",
                messageSource.getMessage("shainSearch.delete", null, CareerConst.DEFAULT_LOCALE));
        return result;
    }

}
