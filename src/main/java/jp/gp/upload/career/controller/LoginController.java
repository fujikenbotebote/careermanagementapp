package jp.gp.upload.career.controller;

import static jp.gp.upload.career.auth.LoginUser.*;
import static jp.gp.upload.career.constant.CareerConst.*;

import java.net.URI;
import java.util.StringJoiner;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import jp.gp.upload.career.auth.LoginUser;
import jp.gp.upload.career.dto.MstShainExt;
import jp.gp.upload.career.form.LoginForm;
import jp.gp.upload.career.logic.LoginLogic;

@Controller
public class LoginController {

    /** ロガー */
    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private LoginLogic loginLogic;

    @Autowired
    MessageSource messageSource;

    /**
     * ログイン画面初期表示
     * @param session
     * @return
     */
    @RequestMapping("login")
    public String login(HttpSession session) {
        // セッション破棄
        session.invalidate();
        // ログイン画面表示（jsp名称と合わせる）
        return "login";
    }

    /**
     * ログイン確認
     * @param form ログイン画面フォーム
     * @param result バリデーションチェック結果
     * @return
     */
    @RequestMapping("login/auth")
    public ModelAndView auth(@Validated LoginForm form, BindingResult result, HttpSession session,
            UriComponentsBuilder builder) {
        // Formにバリデーションエラーがある場合はエラーメッセージを返す
        if (result.hasErrors()) {
            // エラーメッセージを改行で結合する
            StringJoiner joiner = new StringJoiner(HTML_BR);
            for (ObjectError error : result.getAllErrors()) {
                joiner.add(error.getDefaultMessage());
            }
            ModelAndView mv = new ModelAndView();
            form.setErrorMessage(joiner.toString());
            mv.setViewName(VIEW_NAME_LOGIN);
            mv.getModelMap().put("loginForm", form);
            return mv;
        }
        // アカウントロックチェック
        if (loginLogic.isAccountLocked(form.getShainNo())) {
            ModelAndView mv = new ModelAndView();
            form.setErrorMessage(messageSource.getMessage("login.accountLocked", new Object[] {}, DEFAULT_LOCALE));
            mv.setViewName(VIEW_NAME_LOGIN);
            mv.getModelMap().put("loginForm", form);
            return mv;
        }
        MstShainExt shain = loginLogic.getShainByNoPassword(form.getShainNo(), form.getPassword());
        // 社員情報が取得できない場合はログイン失敗
        if (shain == null) {
            loginLogic.countupLock(form.getShainNo());
            ModelAndView mv = new ModelAndView();
            form.setErrorMessage(
                    messageSource.getMessage("login.incollectNoPassword", new Object[] {}, DEFAULT_LOCALE));
            mv.setViewName(VIEW_NAME_LOGIN);
            mv.getModelMap().put("loginForm", form);
            return mv;
        }
        log.info("ログインに成功しました。社員番号：{}", shain.getShainNo());
        // ロック情報をクリアする
        loginLogic.clearLock(form.getShainNo());
        // セッションにログインユーザ情報を登録する。
        session.setAttribute(SESSION_KEY, new LoginUser(shain));
        // トップ画面の初期表示処理にリダイレクトする
        ModelAndView mv = new ModelAndView();
        URI location = builder.path("/top").build().toUri();
        mv.setViewName("redirect:" + location.toString());
        return mv;
    }

}
