package jp.gp.upload.career.form;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * 社員マスタ検索画面用フォーム
 * @author Fujimoto Kenta
 */
public class ShainSearchForm implements Serializable {

    /** 氏名 */
    @Size(max = 20)
    private String shainName;

    /** 組織名 */
    @Size(max = 20)
    private String soshikiName;

    /** メッセージ */
    private String message;

    public String getShainName() {
        return shainName;
    }

    public String getSoshikiName() {
        return soshikiName;
    }

    public String getMessage() {
        return message;
    }

    public void setShainName(String shainName) {
        this.shainName = shainName;
    }

    public void setSoshikiName(String soshikiName) {
        this.soshikiName = soshikiName;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
