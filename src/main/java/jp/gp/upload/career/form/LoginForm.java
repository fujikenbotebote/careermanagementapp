package jp.gp.upload.career.form;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * ログイン画面用フォーム
 * @author Fujimoto Kenta
 */
public class LoginForm implements Serializable {

	/** 社員番号 */
	@NotBlank
	@Size(max = 20)
	private String shainNo;

	/** パスワード */
	@NotBlank
	@Size(max = 100)
	private String password;

	/** エラーメッセージ */
	private String errorMessage;

	public String getShainNo() {
		return shainNo;
	}

	public String getPassword() {
		return password;
	}

	public void setShainNo(String shainNo) {
		this.shainNo = shainNo;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
