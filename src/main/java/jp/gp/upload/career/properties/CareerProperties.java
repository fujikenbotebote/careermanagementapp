package jp.gp.upload.career.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "career")
public class CareerProperties {

    /** アカウントロック失敗回数 */
    public int lockCount = 5;

    /** アカウントロック期間(分) */
    public int lockMinute = 10;

    public int maxSearchCount = 1000;

}
