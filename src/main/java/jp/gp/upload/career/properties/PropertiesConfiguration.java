package jp.gp.upload.career.properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesConfiguration {

	@Bean
	public CareerProperties getCareerProperties() {
		return new CareerProperties();
	}
}
