package jp.gp.upload.career;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;

@SpringBootApplication
public class CareerManagementAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CareerManagementAppApplication.class, args);
	}

	@Bean
	MessageSource messageSource() {
	    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	    messageSource.setDefaultEncoding("UTF-8");
	    messageSource.addBasenames("message");
	    return messageSource;
	}

}
