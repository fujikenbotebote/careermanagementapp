package jp.gp.upload.career.dto;

/**
 * 社員マスタ登録画面のDTO
 * @author Fujimoto Kenta
 */
public class ShainUpdateDto {

    /** 社員ID */
    private String shainId;

    /** 社員番号 */
    private String shainNo;

    /** 氏名 */
    private String shainName;

    /** 性別 */
    private String sex;

    /** 所属組織ID */
    private String shozokuSoshikiId;

    /** 役職ID */
    private String yakushokuId;

    /** 生年月日 */
    private String birthday;

    /** メールアドレス */
    private String emailAddress;

    /** 電話番号 */
    private String phoneNumber;

    /** 入社日 */
    private String nyushabi;

    /** 退社日 */
    private String taishabi;

    public String getShainId() {
        return shainId;
    }

    public String getShainNo() {
        return shainNo;
    }

    public String getShainName() {
        return shainName;
    }

    public String getSex() {
        return sex;
    }

    public String getShozokuSoshikiId() {
        return shozokuSoshikiId;
    }

    public String getYakushokuId() {
        return yakushokuId;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getNyushabi() {
        return nyushabi;
    }

    public String getTaishabi() {
        return taishabi;
    }

    public void setShainId(String shainId) {
        this.shainId = shainId;
    }

    public void setShainNo(String shainNo) {
        this.shainNo = shainNo;
    }

    public void setShainName(String shainName) {
        this.shainName = shainName;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setShozokuSoshikiId(String shozokuSoshikiId) {
        this.shozokuSoshikiId = shozokuSoshikiId;
    }

    public void setYakushokuId(String yakushokuId) {
        this.yakushokuId = yakushokuId;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setNyushabi(String nyushabi) {
        this.nyushabi = nyushabi;
    }

    public void setTaishabi(String taishabi) {
        this.taishabi = taishabi;
    }

    public ShainUpdateDto() {
        shainId = "";
        shainNo = "";
        shainName = "";
        sex = "";
        shozokuSoshikiId = "";
        yakushokuId = "";
        birthday = "";
        emailAddress = "";
        phoneNumber = "";
        nyushabi = "";
        taishabi = "";

    }
}
