package jp.gp.upload.career.dto;

import jp.gp.upload.career.db.entity.MstShain;

/**
 * 社員マスタエンティティの拡張
 * @author Fujimoto Kenta
 */
public class MstShainExt extends MstShain {

    /** 組織名 */
    private String soshikiName;

    /** 組織属性 */
    private String soshikiZokusei;

    /** 属性名称 */
    private String zokuseiName;

    /** 役職名 */
    private String yakushokuName;

    /** 役職ランク */
    private int yakushokuRank;

    public String getSoshikiName() {
        return soshikiName;
    }

    public String getSoshikiZokusei() {
        return soshikiZokusei;
    }

    public String getZokuseiName() {
        return zokuseiName;
    }

    public String getYakushokuName() {
        return yakushokuName;
    }

    public int getYakushokuRank() {
        return yakushokuRank;
    }

    public void setSoshikiName(String soshikiName) {
        this.soshikiName = soshikiName;
    }

    public void setSoshikiZokusei(String soshikiZokusei) {
        this.soshikiZokusei = soshikiZokusei;
    }

    public void setZokuseiName(String zokuseiName) {
        this.zokuseiName = zokuseiName;
    }

    public void setYakushokuName(String yakushokuName) {
        this.yakushokuName = yakushokuName;
    }

    public void setYakushokuRank(int yakushokuRank) {
        this.yakushokuRank = yakushokuRank;
    }
}
