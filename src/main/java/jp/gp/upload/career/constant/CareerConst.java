package jp.gp.upload.career.constant;

import java.util.Locale;

/**
 * 経歴管理システムに関連する定数を扱うクラス
 * @author Fujimoto Kenta
 */
public class CareerConst {

    /** フラグON */
    public static final String FLG_ON = "1";

    /** フラグOFF */
    public static final String FLG_OFF = "0";

    /** デフォルトロケール */
    public static final Locale DEFAULT_LOCALE = Locale.JAPANESE;

    /** 日時フォーマット */
    public static final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

    /** ミリ秒フォーマット */
    public static final String MILL_SECOND_FORMAT = "yyyy/MM/dd HH:mm:ss.SSS";

    /** 日付フォーマット */
    public static final String DATE_FORMAT = "yyyy/MM/dd";

    public static final String CRLF = "\r\n";

    public static final String HTML_BR = "<br/>";

    /** 画面表示モード登録キー */
    public static final String DISP_MODE_KEY = "dispMode";

    /** 画面表示モード：新規登録 */
    public static final String DISP_MODE_CREATE = "1";

    /** 画面表示モード：更新 */
    public static final String DISP_MODE_UPDATE = "2";

    // 画面
    /** ログイン画面 */
    public static final String VIEW_NAME_LOGIN = "login";

    /** トップ画面 */
    public static final String VIEW_NAME_TOP = "top";

    /** トップ画面 */
    public static final String VIEW_NAME_SHAIN_SEARCH = "shainSearch";

    /** トップ画面 */
    public static final String VIEW_NAME_SHIAN_UPDATE = "shainUpdate";

    // プロパティキー
    /** 検索最大件数 */
    public static final String PROPERTY_KEY_SEARCH_MAX_COUNT = "SEARCH_MAX_COUNT";

    /** アカウントロックまでの失敗回数 */
    public static final String PROPERTY_KEY_LOCK_COUNT = "LOCK_COUNT";

    /** アカウントロック期間(分) */
    public static final String PROPERTY_KEY_LOCK_MINUTE = "LOCK_MINUTE";

    /**
     * ロールENUMクラス
     */
    public enum ROLE {
        /** 管理者 */
        ADMIN(1),
        /** 一般ユーザ */
        USER(2),
        ;

        ROLE(int roleId) {
            this.roleId = roleId;
        }

        private int roleId;

        public int getRoleId() {
            return roleId;
        }
    }

    /**
     * 組織属性ENUMクラス
     */
    public enum ZOKUSEI {
        /** 執行部 */
        SHIKKOUBU("01"),
        /** 営業 */
        EIGYO("02"),
        /** 技術 */
        GIJUTSU("03"),
        /** 総務 */
        SOUMU("04"),
        ;

        ZOKUSEI(String zokusei) {
            this.zokusei = zokusei;
        }

        private String zokusei;

        public String getZokusei() {
            return zokusei;
        }
    }

    /**
     * 役職ENUMクラス
     */
    public enum YAKUSHOKU {
        /*+ 役員 */
        YAKUIN(1),
        /*+ 部長 */
        BUCHO(2),
        /*+ 課長 */
        KACHO(3),
        /*+ 係長 */
        KAKARICHO(4),
        /*+ 主任 */
        SHUNIN(5),
        /*+ 平社員 */
        HIRASHAIN(6),
        ;

        YAKUSHOKU(int yakushokuRank) {
            this.yakushokuRank = yakushokuRank;
        }

        private int yakushokuRank;

        public int getYakushokuRank() {
            return yakushokuRank;
        }
    }
}
