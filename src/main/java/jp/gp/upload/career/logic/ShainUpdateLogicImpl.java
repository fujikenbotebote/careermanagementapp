package jp.gp.upload.career.logic;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jp.gp.upload.career.constant.CareerConst;
import jp.gp.upload.career.db.dao.MstShainDao;
import jp.gp.upload.career.db.dao.MstShainDaoExt;
import jp.gp.upload.career.db.dao.MstSoshikiDaoExt;
import jp.gp.upload.career.db.entity.MstShain;
import jp.gp.upload.career.dto.ShainUpdateDto;

@Component
public class ShainUpdateLogicImpl implements ShainUpdateLogic {

    @Autowired
    private MstShainDao mstShainDao;

    @Autowired
    private MstShainDaoExt mstShainDaoExt;

    @Autowired
    private MstSoshikiDaoExt mstSoshikiDaoExt;

    @Override
    public ShainUpdateDto getShain(int shainId) {
        SimpleDateFormat sdf = new SimpleDateFormat(CareerConst.DATE_FORMAT);
        MstShain shainMst = mstShainDaoExt.selectAvailable(shainId);
        ShainUpdateDto shain = new ShainUpdateDto();
        shain.setShainId(String.valueOf(shainMst.getShainId()));
        shain.setShainNo(shainMst.getShainNo());
        shain.setShainName(shainMst.getShainName());
        shain.setSex(shainMst.getSex());
        shain.setBirthday(sdf.format(shainMst.getBirthday()));
        shain.setEmailAddress(shainMst.getEmailAddress());
        shain.setPhoneNumber(shainMst.getPhoneNumber());
        shain.setNyushabi(sdf.format(shainMst.getNyushabi()));
        if (shainMst.getTaishabi() != null) {
            shain.setTaishabi(sdf.format(shainMst.getTaishabi()));
        }

        return shain;
    }

}
