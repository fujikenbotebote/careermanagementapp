package jp.gp.upload.career.logic;

import static jp.gp.upload.career.constant.CareerConst.ZOKUSEI.EIGYO;
import static jp.gp.upload.career.constant.CareerConst.ZOKUSEI.GIJUTSU;
import static jp.gp.upload.career.constant.CareerConst.ZOKUSEI.SHIKKOUBU;
import static jp.gp.upload.career.constant.CareerConst.ZOKUSEI.SOUMU;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import jp.gp.upload.career.constant.CareerConst;
import jp.gp.upload.career.db.dao.MstShainDaoExt;
import jp.gp.upload.career.dto.MstShainExt;

/**
 * 社員マスタ検索ロジック実装クラス
 * @author Fujimoto Kenta
 */
@Component
public class ShainSearchLogicImpl implements ShainSearchLogic {

    @Autowired
    private MstShainDaoExt mstShainDaoExt;

    @Autowired
    private MessageSource messageSource;

    @Override
    public List<MstShainExt> search(String shainName, String soshikiName, int searchMaxCount) {
        List<MstShainExt> shainList = mstShainDaoExt.selectMstSearch(shainName, soshikiName, searchMaxCount);
        // 所属組織の属性に対応する文言を設定
        // ラムダを使い、listのすべての要素で設定する。
        shainList.forEach(shain -> {
            String soshikiZokusei = shain.getSoshikiZokusei();
            if (soshikiZokusei.equals(EIGYO.getZokusei())) {
                shain.setZokuseiName(
                        messageSource.getMessage("common.zokusei.eigyo", null, CareerConst.DEFAULT_LOCALE));
            } else if (soshikiZokusei.equals(SHIKKOUBU.getZokusei())) {
                shain.setZokuseiName(
                        messageSource.getMessage("common.zokusei.shikkoubu", null, CareerConst.DEFAULT_LOCALE));
            } else if (soshikiZokusei.equals(GIJUTSU.getZokusei())) {
                shain.setZokuseiName(
                        messageSource.getMessage("common.zokusei.gijutu", null, CareerConst.DEFAULT_LOCALE));
            } else if (soshikiZokusei.equals(SOUMU.getZokusei())) {
                shain.setZokuseiName(
                        messageSource.getMessage("common.zokusei.soumu", null, CareerConst.DEFAULT_LOCALE));
            }
        });
        return shainList;
    }

    @Override
    public boolean existsShain(int shainId) {
        try {
            // 検索出来たらtrueを返す
            mstShainDaoExt.selectAvailable(shainId);
            return true;
        } catch (EmptyResultDataAccessException e) {
            // 取得できない場合falseを返す
            return false;
        }
    }

    @Override
    public int delete(int shainId, int loginUserId) {
        return mstShainDaoExt.logicalDelete(shainId, loginUserId);
    }

}
