package jp.gp.upload.career.logic;

import jp.gp.upload.career.dto.ShainUpdateDto;

/**
 * 社員マスタ登録ロジック
 * @author Fujimoto Kenta
 *
 */
public interface ShainUpdateLogic {

    /**
     * 社員情報を取得する
     * @param shainId 社員ID
     * @return
     */
    ShainUpdateDto getShain(int shainId);

}
