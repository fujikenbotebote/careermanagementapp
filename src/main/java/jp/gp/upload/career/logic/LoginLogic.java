package jp.gp.upload.career.logic;

import jp.gp.upload.career.dto.MstShainExt;

public interface LoginLogic {

	/**
	 * 社員番号をもとにアカウントロックされているかチェックする
	 * @param shainNumber 社員番号
	 * @return ロックされている場合はtrueを返す
	 */
	boolean isAccountLocked(String shainNumber);

	/**
	 * 社員番号、パスワードをキーにして社員情報を取得する。
	 * 取得できない場合はnullを返す
	 * @param shainNumber 社員番号
	 * @param plainPassword 暗号化されていないパスワード
	 * @return 社員情報
	 */
	MstShainExt getShainByNoPassword(String shainNumber, String plainPassword);

	/**
	 * ロック情報をクリアする
	 * @param shainNumber 社員番号
	 */
	void clearLock(String shainNumber);

	/**
	 * 失敗回数をカウントアップする。
	 * 失敗回数が一定に達した場合、アカウントロックを行う。
	 * @param shainNumber 社員番号
	 */
	void countupLock(String shainNumber);

}
