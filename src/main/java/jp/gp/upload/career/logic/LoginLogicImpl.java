package jp.gp.upload.career.logic;

import static jp.gp.upload.career.constant.CareerConst.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jp.gp.upload.career.db.dao.MstShainDaoExt;
import jp.gp.upload.career.dto.MstShainExt;
import jp.gp.upload.career.properties.CareerProperties;
import jp.gp.upload.career.util.PasswordUtil;

@Component
public class LoginLogicImpl implements LoginLogic {

	private static final Logger log = LoggerFactory.getLogger(LoginLogicImpl.class);

	@Autowired
	private MstShainDaoExt mstShainDaoExt;

	@Autowired
	private CareerProperties careerProperties;

	/** 現在のログイン失敗回数を保持するマップ */
	static private Map<String, Integer> failureCountMap = new HashMap<String, Integer>();

	/** アカウントロックマップ */
	static private Map<String, Date> accountLockDateMap = new HashMap<String, Date>();

	@Override
	public boolean isAccountLocked(String shainNo) {
		// 含んでいない場合は
		if (!accountLockDateMap.containsKey(shainNo)) {
			return false;
		}
		Date lock = accountLockDateMap.get(shainNo);
		// システム日付よりも後の場合、エラーとする
		if (lock.after(new Date())) {
			return true;
		}
		// ロック期限を過ぎている場合はクリアする
		accountLockDateMap.remove(shainNo);
		log.info("ロックを解除します。社員番号:{}", shainNo);
		return false;
	}

	@Override
	public MstShainExt getShainByNoPassword(String shainNo, String plainPassword) {
		// パスワードを暗号化する
		String encodedPassword = PasswordUtil.encode(plainPassword);
		log.info("パスワード:{}", encodedPassword);
		// 社員番号、パスワードをキーにして社員情報を取得する
		return mstShainDaoExt.selectByNoAndPassword(shainNo, encodedPassword);
	}

	@Override
	public void countupLock(String shainNo) {
		int count = 0;
		if (failureCountMap.containsKey(shainNo)) {
			count = failureCountMap.get(shainNo);
		}
		count++;
		log.info("ログイン失敗回数:{}回。社員番号：{}", count, shainNo);
		// ロック失敗回数に達した場合、アカウントロックを行う。
		if (count > careerProperties.lockCount) {
			failureCountMap.remove(shainNo);
			long minute = careerProperties.lockMinute * 1000 * 60;
			Date lock = new Date (System.currentTimeMillis() + minute);
			log.info("アカウントをロックしました。社員番号：{}, 期限：{}", shainNo, new SimpleDateFormat(DATETIME_FORMAT));
			accountLockDateMap.put(shainNo, lock);
			return;
		}
		// 失敗回数を保持
		failureCountMap.put(shainNo, count);
	}

	@Override
	public void clearLock(String shainNo) {
		log.info("ログイン失敗回数をクリアしました。社員番号：{0}", shainNo);
		failureCountMap.remove(shainNo);
	}

}
