package jp.gp.upload.career.logic;

import java.util.List;

import jp.gp.upload.career.dto.MstShainExt;

/**
 * 社員マスタ検索ロジックインターフェース
 * @author Fujimoto Kenta
 */
public interface ShainSearchLogic {

    /**
     * 社員マスタ検索ロジック
     * @param shainName 氏名
     * @param soshikiName 組織名
     * @param searchMaxCount 検索最大件数
     * @return
     */
    List<MstShainExt> search(String shainName, String soshikiName, int searchMaxCount);

    /**
     * 社員の存在チェックを行う
     * @param shainId 社員ID
     * @return 存在する場合trueを返す
     */
    boolean existsShain(int shainId);

    /**
     * 社員情報を削除する
     * @param shainId 社員ID
     * @param loginUserId ログインユーザID
     * @return 削除件数
     */
    int delete(int shainId, int loginUserId);

}
