<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page session="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<base href="${pageContext.request.contextPath}/">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="css/ress.css">
<link rel="stylesheet" href="css/career.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.structure.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.theme.css">
<script type="text/javascript" src="script/jquery-3.5.1.js"></script>
<script type="text/javascript" src="script/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/login.js"></script>
<title><spring:message code="common.system_name" /> - <spring:message code="top.title"/></title>
</head>
<body>
<jsp:include page="/WEB-INF/views/common/header.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/common/menu.jsp"></jsp:include>
<div id="main-container" style="display: inline-block; vertical-align: top; padding: 10px;">
	<div id="page-title" >
		<h2>
			<spring:message code="top.title" />
		</h2>
	</div>
	<div class="search-condition">
	</div>
	<div class=""><spring:message code="top.oshirase" /></div>
	<%-- TODO --%>
</div>
</body>
</html>