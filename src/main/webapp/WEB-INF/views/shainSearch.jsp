<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page session="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<base href="${pageContext.request.contextPath}/">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="css/ress.css">
<link rel="stylesheet" href="css/career.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.structure.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.theme.css">
<script type="text/javascript" src="script/jquery-3.5.1.js"></script>
<script type="text/javascript" src="script/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/shainSearch.js"></script>
<title><spring:message code="common.system_name" /> - <spring:message code="shainSearch.title" /></title>
</head>
<body>
<jsp:include page="/WEB-INF/views/common/header.jsp"></jsp:include>
<div id="content-wrapper" >
<jsp:include page="/WEB-INF/views/common/menu.jsp"></jsp:include>
<div id="main-container" style="display: inline-block; vertical-align: top; padding: 10px;">
	<div id="page-title" >
		<h2>
			<spring:message code="shainSearch.title" />
		</h2>
	</div>
	<div class="search-condition">
		<form name="shainSearchForm" id="shainSearchForm">
			<div><spring:message code="common.searchCondition" /></div>
			<div style="">
				<spring:message code="shainSearch.shainName" />
				<input name="shainName" maxlength="20" >
				<spring:message code="shainSearch.soshikiName" />
				<input name="soshikiName" maxlength="20" >
			</div>
			<div>
				<button id="searchButton" type="button" class="ui-button"><spring:message code="common.searchButton" /></button>
			</div>
		</form>
	</div>
	<div>
	<form name="shainUpdateForm" id="shainUpdateForm" action="shainUpdate" method="post">
		<div style="height: 300px">
			<spring:message code="common.searchResult" />
			<%-- いったん隠しておいて、検索出来たら表示 --%>
			<table id="searchResult" style="display: none;">
				<thead>
					<tr>
						<th><spring:message code="common.select"/></th>
						<th><spring:message code="common.shain_number"/></th>
						<th><spring:message code="shainSearch.shainName"/></th>
						<th><spring:message code="shainSearch.shozokuSoshiki"/></th>
						<th><spring:message code="shainSearch.shainShubetsu"/></th>
					</tr>
				</thead>
				<tbody>
				<%-- 検索後に作成する --%>
				</tbody>
			</table>
		</div>
		<div>
			<button type="button" id="insertButton" class="ui-button"><spring:message code="common.insertButton" /></button>
			<button type="button" id="deleteButton" class="ui-button"><spring:message code="common.deleteButton" /></button>
			<button type="button" id="updateButton" class="ui-button"><spring:message code="common.updateButton" /></button>
		</div>
		<input type="hidden" name="shainId" id="shainId" />
	</form>
	</div>
</div>
</div>
</body>
</html>