<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page session="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<base href="${pageContext.request.contextPath}/">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="css/ress.css">
<link rel="stylesheet" href="css/career.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.structure.css">
<link rel="stylesheet" href="script/jquery-ui-1.12.1/jquery-ui.theme.css">
<script type="text/javascript" src="script/jquery-3.5.1.js"></script>
<script type="text/javascript" src="script/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/login.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/views/common/header.jsp"></jsp:include>
<div id="main-container" style="display: inline-block; vertical-align: top; padding: 10px;">
<div id="page-title" >
  <h2><spring:message code="login.title" /></h2>
</div>
<form action="login/auth" name="loginForm" method="post" id="loginForm">
	<%-- エラーメッセージが存在する場合は表示する。 --%>
	<c:if test="${not empty loginForm && not empty loginForm.errorMessage}">
	<div class="error">
		${loginForm.errorMessage}
	</div>
	</c:if>
	<c:if test="${not empty message}">
	<div class="">
		${message}
	</div>
	</c:if>
	<div><spring:message code="common.shain_number" />：<input type="text" id="shainNo" name="shainNo" value="<c:out value="${loginForm.shainNo}" />"></div>
	<div><spring:message code="login.password" />：<input type="password" id="password" name="password"></div>
	<div><button type="button" id="loginButton" class="ui-button"><spring:message code="login.loginButton" /></button></div>
</form>
</div>
</body>
</html>