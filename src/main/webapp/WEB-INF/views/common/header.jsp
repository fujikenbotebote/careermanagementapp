<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<header id="header" style="width: 100%; display: inline-table; background: aqua;height: 50px; padding: 10px;">
<nav id="appbar">
  <div id="appbar-title" style="display: inline-block; width: 80%; font-size: x-large; font-weight: bolder;">
    <c:choose>
      <c:when test="${empty sessionScope.LoginUserSessionKey}">
        <spring:message code="common.system_name" />
      </c:when>
      <c:otherwise>
        <a class="appbar-brand" href="top"><spring:message code="common.system_name" /></a>
      </c:otherwise>
    </c:choose>
  </div>
  <div id="appbar-name" style="display: inline-block; margin: auto; ">
    <c:out value="${sessionScope.LoginUserSessionKey.shainName}"></c:out>
  </div>
</nav>
</header>