<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="sidedrawer" style="display: inline-block ; max-width: 200px; background-color: gray;">
<nav id="sidenav" style="clear: both;">
  <ul class="ui-menu">
    <c:if test="${sessionScope.LoginUserSessionKey.eigyo || sessionScope.LoginUserSessionKey.kakaricho}">
      <li class="ui-menu-item"><a><spring:message code="menu.shainList"/></a></li>
    </c:if>
    <li class="ui-menu-item"><a><spring:message code="menu.keireki"/></a></li>
    <c:if test="${sessionScope.LoginUserSessionKey.eigyo || sessionScope.LoginUserSessionKey.kakaricho}">
      <li class="ui-menu-item"><a><spring:message code="menu.skillShainList"/></a></li>
    </c:if>
    <c:if test="${sessionScope.LoginUserSessionKey.eigyo || sessionScope.LoginUserSessionKey.kakaricho}">
      <li class="ui-menu-item"><a href="shainSearch"><spring:message code="menu.shainManage"/></a></li>
      <li class="ui-menu-item"><a><spring:message code="menu.soshikiManage"/></a></li>
    </c:if>
    <c:if test="${sessionScope.LoginUserSessionKey.eigyo || sessionScope.LoginUserSessionKey.yakuin}">
      <li class="ui-menu-item"><a><spring:message code="menu.yakushokuManage"/></a></li>
    </c:if>
    <c:if test="${sessionScope.LoginUserSessionKey.admin}">
      <li class="ui-menu-item"><a><spring:message code="menu.oshiraseManage"/></a></li>
    </c:if>
    <li class="ui-menu-item"><a href="logout"><spring:message code="menu.logout"/></a></li>
  </ul>
</nav>
</div>