function tableCreate(list) {
  // すでに検索済みの場合、削除
  $('#searchResult').css('display', 'table')
  let $table = $('#searchResult');
  // 検索結果の数だけ行を作成する
  $.each(list, function (index, value) {
    let $tr = $('<tr></tr>'); // 行作成
    let $tdSelect = $('<td></td>').append($('<input></input>', {"type":"radio", "name" : "selectId", "value" : value.shainId}));
    let $tdNo = $('<td></td>').html(value.shainNo); // 社員番号
    let $tdName = $('<td></td>').html(value.shainName); // 社員名
    let $tdSoshikiName = $('<td></td>').html(value.soshikiName); // 組織名
    let $tdZokusei = $('<td></td>').html(value.zokuseiName); // 組織属性
    $tr.append($tdSelect)
      .append($tdNo)
      .append($tdName)
      .append($tdSoshikiName)
      .append($tdZokusei);
    $table.find('tbody').append($tr);
  });
}

$(document).ready(function() {
  // 検索ボタン
  $('#searchButton').on('click', function() {
    let $table = $('#searchResult');
    // 前回の検索結果がある場合は削除
    $table.find('tbody > tr').remove();
    // AJAXと呼ばれる非同期通信技術を用いて
    // 画面遷移せずに社員情報を検索する
    $.ajax({
      type: "POST",
      url: "shainSearch/search",
      data: $('#shainSearchForm').serialize(),
      dataType:"json",
      success: function(res){
        // 通信が成功した場合に呼ばれる
        // 検索0件エラーがある場合も呼ばれる
        if (res.message){
          $("<div>" + res.message + "</div>").dialog({
            modal: true,
            title:"エラー",
            buttons:{
              "確認": function() {
                $(this).dialog("close");
              }
            }
          });
          return;
        }
        // 正常に取得できる場合
        tableCreate(res.searchResult);
      }
    });
  });
  // 新規登録ボタン
  $('#insertButton').on('click', function() {
    $('#shainUpdateForm').submit();
  });
  // 更新ボタン
  $('#updateButton').on('click', function() {
    let checked = $('[name="selectId"]:checked');
    if (checked.length === 0) {
      // 選択がない場合はエラーを表示して終了する
      $("<div>社員が選択されていません。</div>").dialog({
        modal: true,
        title:"エラー",
        buttons:{
          "確認": function() {
            $(this).dialog("close");
          }
        }
      });
      return;
    }
    $('#shainId').val(checked.val());
    $('#shainUpdateForm').submit();
  });
  // 削除ボタン
  $('#deleteButton').on('click', function() {
    let checked = $('[name="selectId"]:checked');
    if (checked.length === 0) {
      // 選択がない場合はエラーを表示して終了する
      $("<div>社員が選択されていません。</div>").dialog({
        modal: true,
        title:"エラー",
        buttons:{
          "確認": function() {
            $(this).dialog("close");
          }
        }
      });
      return;
    }

    function doDelete() {
      $.ajax({
        type: "POST",
        url: "shainSearch/delete",
        data: "shainId=" + checked.val(),
        dataType: "json",
        success: function(res){
          // 通信が成功した場合に呼ばれる
          // 検索0件エラーがある場合も呼ばれる
          if (res.errorMessage){
            $("<div>" + res.errorMessage + "</div>").dialog({
              modal: true,
              title:"エラー",
              buttons:{
                "確認": function() {
                  $(this).dialog("close");
                }
              }
            });
            return;
          }
          $("<div>" + res.message + "</div>").dialog({
            modal: true,
            title:"情報",
            buttons:{
              "OK": function() {
                $('#searchButton').click();
                $(this).dialog("close");
              }
            }
          });
          return;
        }
      });
    }
    $("<div>削除しますか？</div>").dialog({
      modal: true,
      title:"確認",
      buttons:{
        "OK": function() {
          doDelete();
          $(this).dialog("close");
        },
        "キャンセル": function() {
          $(this).dialog("close");
        }
      }
    });
  });
});