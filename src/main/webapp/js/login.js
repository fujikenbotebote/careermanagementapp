$(document).ready(function() {
  $('#loginButton').on('click', function() {
    if ($('#shainNumber').val()==="") {
      $("<div>社員番号を入力してください。</div>").dialog({
        modal: true,
        title:"エラー",
        buttons:{
          "確認": function() {
            $(this).dialog("close");
          }
        }
      });
      return;
    }
    if ($('#password').val()==="") {
      $("<div>パスワードを入力してください。</div>").dialog({
        modal: true,
        title:"エラー",
        buttons:{
          "確認": function() {
            $(this).dialog("close");
          }
        }
      });
      return;
    }
    $('#loginForm').submit();
  });
});